# Protocol Extension

## Configuration

1. Create a folder where you store the protocols.
2. Create a folder for frontend users, if it does not exist yet. You will need a frontend user group for employees, 
which will be able to create protocols, and one for normal users with read access only.
3. Create a page containing the protocol plugin and choose the created folder as the record storage page.
4. Edit the necessary constants for the protocol extension:
    1. UID of the Frontend Users Folder
    2. UID of the Frontend Login Page
    3. UID of the Frontend User Group (normal user)
    4. UID of the Employee Group
5. Configure Mailjet in the extension configuration
    1. Mailjet API Key
    2. Mailjet API Secret
    3. Mailjet Template ID
    4. Mailjet Sender Mail
    5. Mailjet Sender Name

## How to use it

- Customers and projects must be created in the backend. While creating a customer, you can assign users
which will have access to this specific customer and its projects.
- After creating a customer and a project, you can create a protocol in the frontend while you are in a project.
- A protocol consists of a title, a date, the participants and a description.
- The users that are assigned to a customer can be chosen as participants of a protocol.
- The mailing list can be extended, while the added users will not get access to the whole customer and its projects, but
only to the assigned protocol.
- Now you have the option to save the protocol as draft or to preview it and send a mail containing the protocol to all
the participants afterwards.
- After sending the mails to the participants, the protocol is marked as "sent" and therefore cannot be edited anymore.
If that is the case, you still can extend the mailing list in the protocols detailed view.