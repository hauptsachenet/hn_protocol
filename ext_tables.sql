CREATE TABLE tx_protocol_domain_model_customer (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) NOT NULL DEFAULT '0',
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	logo int(11) unsigned DEFAULT '0' NOT NULL,
    users int(11) unsigned NOT NULL DEFAULT '0',
    projects int(11) unsigned NOT NULL DEFAULT '0',

    PRIMARY KEY (uid),
    KEY pid (pid),
    KEY sorting (pid,sorting)
);

CREATE TABLE tx_protocol_domain_model_project (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) NOT NULL DEFAULT '0',
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
    protocols int(11) unsigned NOT NULL DEFAULT '0',
    customer int(11) unsigned NOT NULL DEFAULT '0',

    PRIMARY KEY (uid),
    KEY pid (pid),
    KEY sorting (pid,sorting)
);

CREATE TABLE tx_protocol_domain_model_protocol (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) NOT NULL DEFAULT '0',
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
    p_date int(11) unsigned NOT NULL DEFAULT '0',
    project int(11) unsigned NOT NULL DEFAULT '0',
    participants int(11) unsigned NOT NULL DEFAULT '0',
    recorder int(11) unsigned DEFAULT NULL,
    description text,
	hash varchar(255) DEFAULT '' NOT NULL,
    draft smallint(6) NOT NULL DEFAULT '0',
    mail_sent int(11) unsigned NOT NULL DEFAULT '0',

    PRIMARY KEY (uid),
    KEY pid (pid),
    KEY sorting (pid,sorting)
);

CREATE TABLE tx_protocol_protocol_user_mm(
    uid_local int(11) DEFAULT '0' NOT NULL,
    uid_foreign int(11) DEFAULT '0' NOT NULL,
    sorting int(11) DEFAULT '0' NOT NULL,

    KEY uid_local (uid_local),
    KEY uid_foreign (uid_foreign)
);

CREATE TABLE tx_protocol_customer_user_mm(
    uid_local int(11) DEFAULT '0' NOT NULL,
    uid_foreign int(11) DEFAULT '0' NOT NULL,
    sorting int(11) DEFAULT '0' NOT NULL,

    KEY uid_local (uid_local),
    KEY uid_foreign (uid_foreign)
);