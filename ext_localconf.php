<?php
defined('TYPO3_MODE') || die('Access denied.');


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Hn.Protocol',
    'protocol',
    [
        'Customer' => 'index',
        'Project' => 'show',
        'Protocol' => 'show, new, create, edit, update, updateParticipants, preview, sendMail',
        'User' => 'new, edit, update, create',
    ],
    [
        'Customer' => 'index',
        'Project' => 'show',
        'Protocol' => 'show, new, create, edit, update, updateParticipants, preview, sendMail',
        'User' => 'new, edit, update, create',
    ]
);