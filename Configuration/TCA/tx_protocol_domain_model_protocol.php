<?php

return [
    'ctrl' => [
        'title' => 'Protocol',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'searchFields' => 'title',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
    ],
    'columns' => [
        'hidden' => $GLOBALS['TCA']['tt_content']['columns']['hidden'],
        'title' => [
            'label' => 'Title',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim, required'
            ]
        ],
        'p_date' => [
            'exclude' => 1,
            'label' => 'Date',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => time(),
            ],
        ],
        'project' => [
            'label' => 'Project',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_protocol_domain_model_project',
                'foreign_record_defaults' => [
                    'colPos' => '99'
                ],
                'enableMultiSelectFilterTextfield' => true,
                'maxitems' => 1,
                'fieldControl' => [
                    'addRecord' => [
                        'disabled' => false,
                        'options' => [
                            'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:file_mountpoints_add_title',
                            'setValue' => 'prepend',
                        ],
                    ],
                ]
            ],
        ],
        'participants' => [
            'label' => 'Participants',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'fe_users',
                'MM' => 'tx_protocol_protocol_user_mm',
                'enableMultiSelectFilterTextfield' => true,
                'fieldControl' => [
                    'addRecord' => [
                        'disabled' => false,
                        'options' => [
                            'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:file_mountpoints_add_title',
                            'setValue' => 'prepend',
                        ],
                    ],
                ]
            ],
        ],
        'recorder' => [
            'label' => 'Recorder',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'fe_users',
                'eval' => 'required',
            ],
        ],
        'description' => [
            'label' => 'Description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
            ],
        ],
        'hash' => [
            'label' => 'Hash',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim, required'
            ]
        ],
        'draft' => [
            'exclude' => true,
            'label' => 'Draft',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => ''
                    ]
                ]
            ]
        ],
        'mail_sent' => [
            'exclude' => 1,
            'label' => 'Mail sent date',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
            ],
        ],
    ],
    'palettes' => [
        'hidden' => [
            'showitem' => '
                hidden;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:field.default.hidden,
                draft
            ',
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => '
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    title, 
                    project,
                    p_date,
                    recorder,
                    participants,
                    description,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden'
        ]
    ]
];