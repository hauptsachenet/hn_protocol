<?php

return [
    'ctrl' => [
        'title' => 'Customer',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'searchFields' => 'title',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
    ],
    'columns' => [
        'hidden' => $GLOBALS['TCA']['tt_content']['columns']['hidden'],
        'title' => [
            'label' => 'Title',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim, required'
            ]
        ],
        'logo' => [
            'label' => 'Logo',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                ],
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'])
        ],
        'users' => [
            'label' => 'Users',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'fe_users',
                'MM' => 'tx_protocol_customer_user_mm',
                'enableMultiSelectFilterTextfield' => true,
                'fieldControl' => [
                    'addRecord' => [
                        'disabled' => false,
                        'options' => [
                            'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:file_mountpoints_add_title',
                            'setValue' => 'prepend',
                        ],
                    ],
                ]
            ],
        ],
        'projects' => [
            'label' => 'Projects',
            'exclude' => 1,
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_protocol_domain_model_project',
                'foreign_field' => 'customer',
                'foreign_sortby' => 'sorting',
                'maxitems' => 100,
                'behaviour' => [
                    'enableCascadingDelete' => TRUE,
                ],
            ]
        ]
    ],
    'palettes' => [
        'hidden' => [
            'showitem' => '
                hidden;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:field.default.hidden
            ',
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => '
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    title, 
                    logo,
                    users,
                    projects,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden'
        ]
    ]
];
