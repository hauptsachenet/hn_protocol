<?php

return [
    'ctrl' => [
        'title' => 'Project',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'searchFields' => 'title',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
    ],
    'columns' => [
        'hidden' => $GLOBALS['TCA']['tt_content']['columns']['hidden'],
        'title' => [
            'label' => 'Title',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim, required'
            ]
        ],
        'customer' => [
            'label' => 'Customer',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_protocol_domain_model_customer',
                'foreign_record_defaults' => [
                    'colPos' => '99'
                ],
                'enableMultiSelectFilterTextfield' => true,
                'maxitems' => 1,
                'fieldControl' => [
                    'addRecord' => [
                        'disabled' => false,
                        'options' => [
                            'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:file_mountpoints_add_title',
                            'setValue' => 'prepend',
                        ],
                    ],
                ]
            ],
        ],
        'protocols' => [
            'label' => 'Protocols',
            'exclude' => 1,
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_protocol_domain_model_protocol',
                'foreign_field' => 'project',
                'foreign_default_sortby' => 'p_date',
                'maxitems' => 100,
                'behaviour' => [
                    'enableCascadingDelete' => TRUE,
                ],
            ]
        ]
    ],
    'palettes' => [
        'hidden' => [
            'showitem' => '
                hidden;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:field.default.hidden
            ',
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => '
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    title, 
                    customer,
                    protocols,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden'
        ]
    ]
];