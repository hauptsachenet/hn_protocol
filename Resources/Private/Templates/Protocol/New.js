if($('#protocol__form').length > 0) {
    ClassicEditor
        .create(document.querySelector('#protocol__form-description'), {
            heading: {
                options: [
                    { model: 'paragraph', title: 'Absatz', class: 'ck-heading_paragraph' },
                    { model: 'heading2', view: 'h2', title: 'Überschrift 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Überschrift 3', class: 'ck-heading_heading3' },
                ]
            },
            toolbar: ['heading', '|', 'bold', 'italic', '|', 'bulletedList', 'numberedList', '|', 'link'],
            language: 'de'
        })
        .catch(error => {
            console.error(error);
        });

    $('#protocol__form-date').flatpickr({
        enableTime: true,
        dateFormat: 'd.m.Y H:i',
        time_24hr: true,
        locale: 'de',
        weekNumbers: true
    });

    var selectizeOptions = {
        persist: false,
        maxItems: null,
        plugins: ['remove_button'],
        render: {
            option_create: function(data, escape) {
                return '<div class="create">Füge <strong>' + escape(data.input) + '</strong> hinzu &hellip;</div>';
            }
        },
    };

    var selectizeRender = {
        option_create: function(data, escape) {
            return '<div class="create">Füge <strong>' + escape(data.input) + '</strong> hinzu &hellip;</div>';
        }
    };

    $('.protocol__form-participants').selectize({
        persist: false,
        maxItems: null,
        plugins: ['remove_button'],
        render: selectizeRender,
    });
    $('.protocol__form-mailing-list').selectize({
        persist: false,
        maxItems: null,
        plugins: ['remove_button'],
        create: true,
        render: selectizeRender,
    });
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});