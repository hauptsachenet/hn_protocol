<?php

namespace Hn\Protocol\ViewHelpers;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class IsParticipantViewHelper extends AbstractViewHelper
{
    /**
     * Returns true if the given user is an actual participant of the protocol
     */

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('participants', 'array', 'All participants');
        $this->registerArgument('user', 'object', 'The user');
    }

    public function render()
    {
        /** @var array $participants */
        $participants = $this->arguments['participants'];
        $user = $this->arguments['user'];

        return in_array($user, $participants, true);
    }
}
