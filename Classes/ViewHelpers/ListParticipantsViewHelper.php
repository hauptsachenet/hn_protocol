<?php

namespace Hn\Protocol\ViewHelpers;

use Hn\Protocol\Domain\Model\Protocol;
use Hn\Protocol\Service\ProtocolService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class ListParticipantsViewHelper extends AbstractViewHelper
{
    /**
     * Returns a comma separated list of all participants of a protocol
     */

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('protocol', 'object', 'The protocol');
    }

    public function render()
    {
        /** @var Protocol $protocol */
        $protocol = $this->arguments['protocol'];

        return GeneralUtility::makeInstance(ObjectManager::class)->get(ProtocolService::class)->listParticipants($protocol->getParticipants(), true, $protocol);
    }
}
