<?php

namespace Hn\Protocol\ViewHelpers\Link;

use Hn\Protocol\Domain\Model\Protocol;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class ProtocolViewHelper extends AbstractTagBasedViewHelper
{

    /**
     * @var \Hn\Protocol\Service\ProtocolService
     * @inject
     */
    protected $protocolService;

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute('name', 'string', 'Specifies the name of an anchor');
        $this->registerTagAttribute('rel', 'string', 'Specifies the relationship between the current document and the linked document');
        $this->registerTagAttribute('rev', 'string', 'Specifies the relationship between the linked document and the current document');
        $this->registerTagAttribute('target', 'string', 'Specifies where to open the linked document');

        $this->registerArgument('pageUid', 'int', 'the page uid', false, $GLOBALS['TSFE']->id);
        $this->registerArgument('protocol', Protocol::class, 'the protocol to link to', true);
    }

    public function render()
    {
        /** @var Protocol $protocol */
        $protocol = $this->arguments['protocol'];
        $pageUid = $this->arguments['pageUid'];

        $uriBuilder = $this->renderingContext->getControllerContext()->getUriBuilder();
        $uri = $uriBuilder
            ->reset()
            ->setTargetPageUid($pageUid)
            ->setCreateAbsoluteUri(true)
            ->uriFor('show', ['hash' => $protocol->getHash()], 'Protocol', 'Protocol', 'Protocol');
        $this->tag->addAttribute('href', $uri);
        $this->tag->setContent($this->renderChildren());
        $this->tag->forceClosingTag(true);
        return $this->tag->render();
    }

}