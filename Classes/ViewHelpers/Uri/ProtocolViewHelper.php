<?php

namespace Hn\Protocol\ViewHelpers\Uri;

use Hn\Protocol\Domain\Model\Protocol;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ProtocolViewHelper extends AbstractViewHelper
{

    /**
     * @var \Hn\Protocol\Service\ProtocolService
     * @inject
     */
    protected $protocolService;

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('pageUid', 'int', 'the page uid', false, $GLOBALS['TSFE']->id);
        $this->registerArgument('protocol', Protocol::class, 'the protocol to link to', true);
    }

    public function render()
    {
        /** @var Protocol $protocol */
        $protocol = $this->arguments['protocol'];
        $pageUid = $this->arguments['pageUid'];

        $uriBuilder = $this->renderingContext->getControllerContext()->getUriBuilder();
        $uri = $uriBuilder
            ->reset()
            ->setTargetPageUid($pageUid)
            ->setCreateAbsoluteUri(true)
            ->uriFor('show', ['hash' => $protocol->getHash()], 'Protocol', 'Protocol', 'Protocol');

        return $uri;
    }

}