<?php

namespace Hn\Protocol\Service;


use Hn\Protocol\Domain\Model\Protocol;
use Hn\Protocol\Utility\AccessUtility;
use Mailjet\Resources;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;

class MailService
{
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $apiSecret;

    /**
     * @var int
     */
    protected $templateId;

    /**
     * @var string
     */
    protected $senderMail;

    /**
     * @var string
     */
    protected $senderName;

    /**
     * @var \Hn\Protocol\Service\ProtocolService
     * @inject
     */
    protected $protocolService;

    /**
     * MailService constructor.
     */
    public function __construct()
    {
        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['protocol']);

        $this->apiKey = $extensionConfiguration['mailjetApiKey'];
        $this->apiSecret = $extensionConfiguration['mailjetApiSecret'];
        $this->templateId = (int)$extensionConfiguration['mailjetTemplateId'];
        $this->senderMail = $extensionConfiguration['mailjetSenderMail'];
        $this->senderName = $extensionConfiguration['mailjetSenderName'];
    }


    /**
     * @param FrontendUser $recipient
     * @param Protocol $protocol
     * @return int
     */
    public function send($recipient, $protocol)
    {
        $mj = new \Mailjet\Client($this->apiKey, $this->apiSecret, true, ['version' => 'v3.1']);

        $variables = [
            'first_name' => $recipient->getFirstName(),
            'last_name' => $recipient->getLastName(),
            'protocol_title' => $protocol->getTitle(),
            'protocol_description' => $protocol->getDescription(),
            'protocol_date' => $protocol->getPDate()->format('d.m.Y - H:i'),
            'protocol_participants' => $this->protocolService->listParticipants($protocol->getParticipants(), true, $protocol),
            'can_login' => AccessUtility::canLogin($recipient)
        ];

        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $this->senderMail,
                        'Name' => $this->senderName
                    ],
                    'To' => [
                        [
                            'Email' => $recipient->getEmail()
                        ],
                    ],
                    'TemplateID' => $this->templateId,
                    'TemplateLanguage' => true,
                    'Subject' => "Protokoll: " . $protocol->getTitle(),
                    'Variables' => $variables
                ]
            ]
        ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response->success();
    }
}