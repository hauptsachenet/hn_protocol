<?php


namespace Hn\Protocol\Service;


use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;

class UserService
{

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * @param string $email
     * @param $frontendUsersFolder
     * @return FrontendUser
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function createUser($email, $frontendUsersFolder)
    {
        $user = new FrontendUser();
        $user->setUsername($email);
        $user->setEmail($email);
        $user->setPid($frontendUsersFolder);

        $this->frontendUserRepository->add($user);

        return $user;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function userExists($email)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('fe_users')->createQueryBuilder();

        return $queryBuilder
            ->select('*')
            ->from('fe_users')
            ->where(
                $queryBuilder->expr()->eq('email', $queryBuilder->createNamedParameter($email))
            )
            ->execute()
            ->fetch();
    }
}