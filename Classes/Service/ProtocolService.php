<?php


namespace Hn\Protocol\Service;


use Hn\Protocol\Domain\Model\Protocol;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;

class ProtocolService
{
    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * @var \Hn\Protocol\Service\UserService
     * @inject
     */
    protected $userService;

    /**
     * @param Protocol $protocol
     * @param $participant
     * @param $frontendUsersFolder
     * @return mixed
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function addParticipant(Protocol $protocol, $participant, $frontendUsersFolder)
    {
        if ($user = $this->userService->userExists($participant)) {
            /** @var FrontendUser $user */
            $user = $this->frontendUserRepository->findByUid($user['uid']);
        } else {
            $user = $this->userService->createUser($participant, $frontendUsersFolder);
        }

        $protocol->addParticipant($user);

        return $user;
    }

    /**
     * returns comma separated list of the participants
     *
     * @param $users
     * @param bool $includeRecorder
     * @param Protocol $protocol
     * @return string
     */
    public function listParticipants($users, $includeRecorder = false, $protocol = null)
    {
        $mailingList = [];

        if ($includeRecorder) {
            $mailingList[] = $protocol->getRecorder()->getFirstName() . ' ' . $protocol->getRecorder()->getLastName();
        }

        /** @var FrontendUser $user */
        foreach ($users as $user) {
            if ($user->getFirstName() && $user->getLastName()) {
                $mailingList[] = $user->getFirstName() . ' ' . $user->getLastName();
            } elseif ($user->getLastName()) {
                $mailingList[] = $user->getLastName();
            } else {
                $mailingList[] = $user->getEmail();
            }
        }

        return implode(', ', $mailingList);
    }

    /**
     * @param $length
     * @return string
     */
    public function generateRandomString($length)
    {
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, count($characters) - 1)];
        }

        return $randomString;
    }
}