<?php

namespace Hn\Protocol\Utility;

use Hn\Protocol\Domain\Model\Project;
use Hn\Protocol\Exception\AccessDeniedException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class AccessUtility
{


    /**
     * @return FrontendUser
     * @throws AccessDeniedException
     */
    public static function getCurrentUser(): FrontendUser
    {
        if ($uid = $GLOBALS['TSFE']->fe_user->user['uid']) {
            return GeneralUtility::makeInstance(ObjectManager::class)->get(FrontendUserRepository::class)->findByUid($uid);
        }

        throw new AccessDeniedException();
    }

    /**
     * @param Project $project
     * @return bool
     * @throws AccessDeniedException
     */
    public static function hasAccess(Project $project)
    {
        if ($project->getCustomer()->getUsers()->contains(self::getCurrentUser())) {
            return true;
        }

        throw new AccessDeniedException();
    }

    /**
     * check if user can login and has access to a project
     *
     * @param FrontendUser $user
     * @return int
     */
    public static function canLogin(FrontendUser $user)
    {
        if ($user->getUsergroup()->count() > 0) {
            return true;
        }

        return false;
    }
}