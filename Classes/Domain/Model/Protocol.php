<?php

namespace Hn\Protocol\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Protocol extends AbstractEntity
{

    /**
     * @var string
     */
    protected $title;

    /**
     * @var \Hn\Protocol\Domain\Model\Project
     */
    protected $project;

    /**
     * @var \DateTime
     */
    protected $pDate;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FrontendUser>
     */
    protected $participants;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $recorder;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $hash;

    /**
     * @var bool
     */
    protected $draft;

    /**
     * @var \DateTime
     */
    protected $mailSent;

    /**
     * Protocol constructor.
     */
    public function __construct()
    {
        $this->participants = new ObjectStorage();
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project): void
    {
        $this->project = $project;
    }

    /**
     * @return \DateTime
     */
    public function getPDate(): \DateTime
    {
        return $this->pDate;
    }

    /**
     * @param \DateTime $pDate
     */
    public function setPDate(\DateTime $pDate): void
    {
        $this->pDate = $pDate;
    }

    /**
     * @return array
     */
    public function getParticipants(): array
    {
        return $this->participants->toArray();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $participants
     */
    public function setParticipants(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $participants): void
    {
        $this->participants = $participants;
    }

    public function addParticipant(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $frontendUser)
    {
        $this->participants->attach($frontendUser);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    public function getRecorder(): \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
    {
        return $this->recorder;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $recorder
     */
    public function setRecorder(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $recorder): void
    {
        $this->recorder = $recorder;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->draft;
    }

    /**
     * @param bool $draft
     */
    public function setDraft(bool $draft): void
    {
        $this->draft = $draft;
    }

    /**
     * @return \DateTime
     */
    public function getMailSent(): ?\DateTime
    {
        return $this->mailSent;
    }

    /**
     * @param \DateTime $mailSent
     */
    public function setMailSent(\DateTime $mailSent): void
    {
        $this->mailSent = $mailSent;
    }
}