<?php


namespace Hn\Protocol\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Customer extends AbstractEntity
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $logo;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FrontendUser>
     */
    protected $users;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hn\Protocol\Domain\Model\Project>
     */
    protected $projects;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->users = new ObjectStorage();
        $this->projects = new ObjectStorage();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getLogo(): ?\TYPO3\CMS\Extbase\Domain\Model\FileReference
    {
        return $this->logo;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getUsers(): \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->users;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $user
     */
    public function addUser(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $user)
    {
        $this->users->attach($user);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $users
     */
    public function setUsers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $users): void
    {
        $this->users = $users;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getProjects(): \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->projects;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $projects
     */
    public function setProjects(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $projects): void
    {
        $this->projects = $projects;
    }
}