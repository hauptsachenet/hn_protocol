<?php

namespace Hn\Protocol\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Project extends AbstractEntity
{

    /**
     * @var string
     */
    protected $title;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hn\Protocol\Domain\Model\Protocol>
     */
    protected $protocols;

    /**
     * @var \Hn\Protocol\Domain\Model\Customer
     */
    protected $customer;

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->protocols = new ObjectStorage();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getProtocols(): \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->protocols;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $protocols
     */
    public function setProtocols(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $protocols): void
    {
        $this->protocols = $protocols;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

}