<?php

namespace Hn\Protocol\Domain\Repository;

use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Persistence\Repository;


class CustomerRepository extends Repository
{
    protected $defaultOrderings = array(
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    /**
     * @param FrontendUser $user
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByUser(FrontendUser $user)
    {
        $query =$this->createQuery();
        $query->matching(
            $query->contains('users', $user)
        );

        return $query->execute();
    }
}