<?php

namespace Hn\Protocol\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class ProtocolRepository
 * @package Hn\Protocol\Domain\Repository
 */
class ProtocolRepository extends Repository
{
    protected $defaultOrderings = array(
        'pDate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
    );

    /**
     * @param string $hash
     * @return object
     */
    public function findByHash($hash)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('hash', $hash)
        );

        return $query->execute()->getFirst();
    }

    public function findByProject($project)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('project', $project)
        );

        return $query->execute();
    }
}