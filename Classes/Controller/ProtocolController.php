<?php

namespace Hn\Protocol\Controller;


use Hn\Protocol\Domain\Model\Project;
use Hn\Protocol\Domain\Model\Protocol;
use Hn\Protocol\Utility\AccessUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter;

class ProtocolController extends ActionController
{

    /**
     * @var \Hn\Protocol\Domain\Repository\ProtocolRepository
     * @inject
     */
    protected $protocolRepository;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;

    /**
     * @var \Hn\Protocol\Service\ProtocolService
     * @inject
     */
    protected $protocolService;

    /**
     * @var \Hn\Protocol\Service\MailService
     * @inject
     */
    protected $mailService;

    /**
     * @param string $hash
     */
    public function showAction(string $hash)
    {
        /** @var Protocol $protocol */
        $protocol = $this->protocolRepository->findByHash($hash);

        $this->view->assign('protocol', $protocol);
    }

    /**
     * @param Project $project
     * @throws \Hn\Protocol\Exception\AccessDeniedException
     */
    public function newAction(Project $project)
    {
        $user = AccessUtility::getCurrentUser();

        $protocol = new Protocol();
        $protocol->setProject($project);
        $protocol->setRecorder($user);
        $protocol->setHash($this->protocolService->generateRandomString(20));
        $protocol->setDraft(true);

        $this->view->assign('protocol', $protocol);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function initializeCreateAction()
    {
        $propertyMappingConfiguration = $this->arguments->getArgument('protocol')->getPropertyMappingConfiguration();
        $propertyMappingConfiguration->forProperty('pDate')
            ->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'd.m.Y H:i');
        $propertyMappingConfiguration->allowProperties('participants');
    }

    /**
     * @param Protocol $protocol
     * @param array $newParticipants
     * @param bool $preview
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function createAction(Protocol $protocol, array $newParticipants = null, $preview = false)
    {
        foreach ($newParticipants as $newParticipant) {
            $this->protocolService->addParticipant($protocol, $newParticipant, $this->settings['frontendUsersFolder']);
        }

        $this->protocolRepository->add($protocol);
        $this->persistenceManager->persistAll();

        if ($preview) {
            $this->redirect('preview', null, null, ['protocol' => $protocol]);
        }

        $this->addFlashMessage('Entwurf gespeichert.', 'Protokoll: ' . $protocol->getTitle());
        $this->redirect('show', 'Project', null, ['project' => $protocol->getProject()]);
    }

    /**
     * @param Protocol $protocol
     * @throws \Hn\Protocol\Exception\AccessDeniedException
     */
    public function editAction(Protocol $protocol)
    {
        AccessUtility::hasAccess($protocol->getProject());

        $this->view->assign('protocol', $protocol);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function initializeUpdateAction()
    {
        $propertyMappingConfiguration = $this->arguments->getArgument('protocol')->getPropertyMappingConfiguration();
        $propertyMappingConfiguration->forProperty('pDate')
            ->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'd.m.Y H:i');
        $propertyMappingConfiguration->allowProperties('participants');
    }

    /**
     * @param Protocol $protocol
     * @param array $newParticipants
     * @param bool $preview
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function updateAction(Protocol $protocol, array $newParticipants = null, $preview = false)
    {
        foreach ($newParticipants as $newParticipant) {
            $this->protocolService->addParticipant($protocol, $newParticipant, $this->settings['frontendUsersFolder']);
        }

        $this->protocolRepository->update($protocol);
        $this->persistenceManager->persistAll();

        if ($preview) {
            $this->redirect('preview', null, null, ['protocol' => $protocol]);
        }

        $this->addFlashMessage('Entwurf gespeichert.', 'Protokoll: ' . $protocol->getTitle());
        $this->redirect('show', 'Project', null, ['project' => $protocol->getProject()]);
    }

    /**
     * @param Protocol $protocol
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function sendMailAction(Protocol $protocol)
    {
        foreach ($protocol->getParticipants() as $participant) {
            $this->mailService->send($participant, $protocol);
        }

        $protocol->setMailSent(new \DateTime());
        $protocol->setDraft(false);
        $this->protocolRepository->update($protocol);

        $this->addFlashMessage('E-Mail versendet an: ' . $this->protocolService->listParticipants($protocol->getParticipants()), 'Protokoll: ' . $protocol->getTitle());
        $this->redirect('show', 'Project', null, ['project' => $protocol->getProject()]);
    }

    /**
     * @param Protocol $protocol
     */
    public function previewAction(Protocol $protocol)
    {
        $this->view->assign('protocol', $protocol);
    }

    /**
     * @param Protocol $protocol
     * @param array $participants
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function updateParticipantsAction(Protocol $protocol, array $participants)
    {
        $users = [];

        foreach ($participants as $participant) {
            $users[] = $this->protocolService->addParticipant($protocol, $participant, $this->settings['frontendUsersFolder']);
        }

        $this->protocolRepository->update($protocol);
        $this->persistenceManager->persistAll();

        /** @var FrontendUser $user */
        foreach ($users as $user) {
            $this->mailService->send($user, $protocol);
        }

        $this->addFlashMessage('E-Mail versendet an: ' . $this->protocolService->listParticipants($users), 'Protokoll: ' . $protocol->getTitle());
        $this->redirect('show', 'Project', null, ['project' => $protocol->getProject()]);
    }
}