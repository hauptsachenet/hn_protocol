<?php

namespace Hn\Protocol\Controller;

use Hn\Protocol\Exception\AccessDeniedException;
use Hn\Protocol\Utility\AccessUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class CustomerController extends ActionController
{
    /**
     * @var \Hn\Protocol\Domain\Repository\CustomerRepository
     * @inject
     */
    protected $customerRepository;

    /**
     * @throws AccessDeniedException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function indexAction()
    {
        $user = AccessUtility::getCurrentUser();
        $customers = $this->customerRepository->findByUser($user);

        if($customers->count() === 1) {
            $customer = $customers->getFirst();
            $projects = $customer->getProjects();

            if($projects->count() === 1) {
                $this->redirect('show', 'Project', 'protocol', ['project' => $projects->current()]);
            }
        }

        $this->view->assign('customers', $customers);
    }
}
