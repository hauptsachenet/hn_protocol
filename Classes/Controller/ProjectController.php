<?php

namespace Hn\Protocol\Controller;

use Hn\Protocol\Domain\Model\Project;
use Hn\Protocol\Domain\Repository\ProtocolRepository;
use Hn\Protocol\Utility\AccessUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class ProjectController extends ActionController
{

    /**
     * @param Project $project
     * @throws \Hn\Protocol\Exception\AccessDeniedException
     */
    public function showAction(Project $project)
    {
        AccessUtility::hasAccess($project);

        $this->view->assign('project', $project);
        $this->view->assign('protocols', GeneralUtility::makeInstance(ObjectManager::class)->get(ProtocolRepository::class)->findByProject($project));
    }
}
