<?php

namespace Hn\Protocol\Controller;


use Hn\Protocol\Domain\Model\Project;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Saltedpasswords\Salt\SaltFactory;

class UserController extends ActionController
{

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository;

    /**
     * @var \Hn\Protocol\Domain\Repository\CustomerRepository
     * @inject
     */
    protected $customerRepository;

    /**
     * persistence manager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;

    public function newAction(Project $project)
    {
        $this->view->assign('project', $project);
    }

    /**
     * @param FrontendUser $user
     * @param Project $project
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function createAction(FrontendUser $user, Project $project)
    {
        /** @var FrontendUserGroup $frontendUserGroup */
        $frontendUserGroup = $this->frontendUserGroupRepository->findByUid($this->settings['frontendUserGroupUid']);
        $user->setUsername($user->getEmail());
        $user->setPassword($this->saltPassword($user->getPassword()));
        $user->addUsergroup($frontendUserGroup);
        $user->setPid($this->settings['frontendUsersFolder']);
        $this->frontendUserRepository->add($user);

        $this->persistenceManager->persistAll();

        $customer = $project->getCustomer();
        $customer->addUser($user);
        $this->customerRepository->update($customer);

        $this->addFlashMessage('User wurde dem Kunden hinzugefügt.');
        $this->redirect('show', 'Project', null, ['project' => $project]);
    }

    /**
     * @param FrontendUser $user
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function editAction(FrontendUser $user)
    {
        if (!empty($user->getPassword())) {
            $this->redirect('showLogin', 'FrontendLogin', 'felogin', null, $this->settings['frontendLoginUid']);
        }

        $this->view->assign('user', $user);
    }

    /**
     * @param FrontendUser $user
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function updateAction(FrontendUser $user)
    {
        /** @var FrontendUserGroup $frontendUserGroup */
        $frontendUserGroup = $this->frontendUserGroupRepository->findByUid($this->settings['frontendUserGroupUid']);

        $user->setPassword($this->saltPassword($user->getPassword()));
        $user->addUsergroup($frontendUserGroup);

        $this->frontendUserRepository->update($user);

        $this->redirect('showLogin', 'FrontendLogin', 'felogin', null, $this->settings['frontendLoginUid']);
    }

    /**
     * @param string $password
     * @return string
     */
    private function saltPassword($password)
    {
        $saltingInstance = SaltFactory::getSaltingInstance();

        return $saltingInstance->getHashedPassword($password);
    }
}