<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Protocol',
    'description' => 'A protocol extension.',
    'category' => 'plugin',
    'author' => 'Marvin Dosse',
    'author_company' => 'hauptsache.net GmbH',
    'author_email' => 'marvin@hauptsache.net',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '1.0.4',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.9.99'
        ]
    ]
];